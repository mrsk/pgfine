#[macro_use] extern crate anyhow;

use clap;
pub mod project;
pub mod database;
pub mod utils;


fn main() -> anyhow::Result<()> {

    let about = format!("{}

ENVIRONMENT VARIABLES:
    PGFINE_DIR                      Project location
    PGFINE_CONNECTION_STRING        Connection string for target database
    PGFINE_ADMIN_CONNECTION_STRING  Connection string for admin database
    PGFINE_ROOT_CERT                Path to root certificate to verify server's certificate
    PGFINE_ROLE_PREFIX              Role prefix to make them unique per environment
",
        clap::crate_description!(),        
    );

    let about_str = about.as_str();

    let mut clap: clap::App = clap::App::new(clap::crate_name!())
    .version(clap::crate_version!())
    .author(clap::crate_authors!("\n"))
    .about(about_str)
    .subcommand(clap::App::new("init")
        .about("Initialize new pgfine project"))
    .subcommand(clap::App::new("migrate")
        .about("Update database"))
    .subcommand(clap::App::new("drop")
        .subcommand(clap::App::new("database")
            .about("Drop entire database"))
        .subcommand(clap::App::new("objects")
            .about("Drop database objects")
            .arg(clap::Arg::new("drop-public-schema")
                .long("drop-public-schema")
                .about("Attempt to drop public schema"))
            .arg(clap::Arg::new("drop-pgfine-tables")
                .long("drop-pgfine-tables")
                .about("Attempt to drop pgfine tables")))
        .about("Drop something")
        .arg(clap::Arg::new("no-joke") // wtf
            .long("no-joke")
            .about("Confirmation")));

    let matches = clap.clone().get_matches();
    
    match matches.subcommand() {
        Some(("init", subcommand_matches)) => {
            utils::validate_environment()?;
            subcommand_init(subcommand_matches)?;
        },
        Some(("migrate", subcommand_matches)) => {
            utils::validate_environment()?;
            subcommand_migrate(subcommand_matches)?;
        },
        Some(("drop", subcommand_matches)) => {
            utils::validate_environment()?;
            let subclap = clap.find_subcommand("drop").expect("drop subcommand");
            subcommand_drop(&subclap, subcommand_matches)?;
        },
        _ => {
            clap.print_long_help()?
        }
    }

    return Ok(());
}


fn subcommand_init(_matches: &clap::ArgMatches) -> anyhow::Result<()> {
    project::init()?;
    return Ok(());
}

fn subcommand_migrate(_matches: &clap::ArgMatches) -> anyhow::Result<()> {
    let database_project = project::load()?;
    database::migrate(database_project)?;
    return Ok(());
}

fn subcommand_drop(clap: &clap::App, matches: &clap::ArgMatches) -> anyhow::Result<()> {
    if !matches.is_present("no-joke") {
        println!("Are you sure? Try with --no-joke argument following 'drop' subcommand");
        return Ok(());
    }


    let subcommand = matches.subcommand();
    match subcommand {
        Some(("database", subcommand_matches)) => {
            subcommand_drop_database(subcommand_matches)?;
        },
        Some(("objects", subcommand_matches)) => {
            subcommand_drop_objects(subcommand_matches)?;
        },
        _ => {
            clap.clone().print_long_help()?;
        }
    }

    return Ok(());
    
}

fn subcommand_drop_objects(matches: &clap::ArgMatches) -> anyhow::Result<()> {
    let drop_public_schema = matches.is_present("drop-public-schema");
    let drop_pgfine_tables = matches.is_present("drop-pgfine-tables");
    let database_project = project::load()?;
    database::drop_objects(&database_project, drop_public_schema, drop_pgfine_tables)?;
    return Ok(());
}

fn subcommand_drop_database(_matches: &clap::ArgMatches) -> anyhow::Result<()> {
    let database_project = project::load()?;
    database::drop_database(&database_project)?;
    return Ok(());
}